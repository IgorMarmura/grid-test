import React, { Component } from 'react';
import stubData from '../../constants/stubData';
import Grid from '../Grid';
import './app.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Grid data={stubData} />
      </div>
    );
  }
}

export default App;
