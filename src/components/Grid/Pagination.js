import React, { Component } from 'react';
import './grid.css';

class Pagination extends Component {

  handleSelectChange = (e) => {
    e.preventDefault();
    this.props.handlePageChange(e.target.value);
  };

  nextPage = () => {
    let nextPage = this.props.currentPage + 1;
    if(nextPage < this.props.pages.length)
      this.props.handlePageChange(nextPage);
  };

  prevPage = () => {
    if(this.props.currentPage !== 0)
      this.props.handlePageChange(this.props.currentPage - 1);
  };

  render() {
    return (
      <div className="pagination-container">
        {this.props.currentPage !== 0 && <span className='pagination-container-prev' onClick={this.prevPage}>Prev</span>}


        <select value={this.props.currentPage}
                onChange={this.handleSelectChange}>
          {this.props.pages.map((item) => <option key={'key' + item}
                                                  value={item}>{item + 1}</option> )}
        </select>

        {(this.props.currentPage + 1 < this.props.pages.length) && <span className='pagination-container-next' onClick={this.nextPage}>Next</span>}
      </div>
    );
  }
}

export default Pagination;