import React, { PureComponent } from 'react';
import './grid.css';

const PAGE_SIZE_TYPES = {
  '10': 10,
  '20': 20,
  '50': 50,
  '100': 100
};

class PageSize extends PureComponent {

  handleOptionChange = (e) => {
    this.props.handlePageSizeChange(e.target.value);
  };

  render() {
    const { currentPageSize } = this.props;

    return (
      <div className="presentation-switch-container">
        <div className="radio">
          <label>
            <input type="radio"
                   value={PAGE_SIZE_TYPES['10']}
                   checked={currentPageSize === PAGE_SIZE_TYPES['10']}
                   onChange={this.handleOptionChange}/>
            10
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio"
                   value={PAGE_SIZE_TYPES['20']}
                   checked={currentPageSize === PAGE_SIZE_TYPES['20']}
                   onChange={this.handleOptionChange}/>
            20
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio"
                   value={PAGE_SIZE_TYPES['50']}
                   checked={currentPageSize === PAGE_SIZE_TYPES['50']}
                   onChange={this.handleOptionChange}/>
            50
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio"
                   value={PAGE_SIZE_TYPES['100']}
                   checked={currentPageSize === PAGE_SIZE_TYPES['100']}
                   onChange={this.handleOptionChange}/>
            100
          </label>
        </div>
      </div>
    );
  }
}

export default PageSize;
