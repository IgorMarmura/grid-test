import React, { Component }    from 'react';
import Table                   from './Table';
import PageSize                from './PageSize';
import Pagination              from './Pagination';
import TABLE_COLUMNS_ENUM      from '../../constants/tableColumnsEnum';
import TABLE_COLUMN_SORT_TYPES from '../../constants/tableColumnsSortTypes';
import { objectsArraySort }    from '../../utils/grid';
import './grid.css';

class Grid extends Component {

  state = {
    currentData      : [],
    currentPage      : 0,
    currentPageSize  : 10,
    currentSort      : TABLE_COLUMN_SORT_TYPES.ASC,
    currentSortColumn: TABLE_COLUMNS_ENUM['id'],
    pages            : []
  };

  constructor() {
    super();
    this.gridScroller = React.createRef();
  }

  componentDidMount() {
    this.handlePageChange(
      this.state.currentPage,
      this.state.currentPageSize,
    );
    this.setPagination(
      this.props.data.length,
      this.state.currentPageSize
    );
  }

  handlePageSizeChange = (size) => {
    size = +size || this.state.size;
    this.setState({
      currentPageSize: size,
      currentPage    : 0
    }, () => {
      this.setCurrentData();
      this.setPagination(
        this.props.data.length,
        size
      )
    });
  };

  handlePageChange = (page) => {
    this.setState({currentPage: +page}, this.setCurrentData);
  };

  handleColumnSort = (column) => {
    let data = [...this.props.data];
    let sort = this.state.currentSort === TABLE_COLUMN_SORT_TYPES.ASC ?
      TABLE_COLUMN_SORT_TYPES.DESC :
      TABLE_COLUMN_SORT_TYPES.ASC;

    data = objectsArraySort(
      data,
      TABLE_COLUMNS_ENUM[column],
      sort
    );

    this.setState({
      currentData      : data,
      currentSort      : sort,
      currentSortColumn: TABLE_COLUMNS_ENUM[column],
    }, this.setCurrentData);
  };

  setCurrentData = () => {
    const {currentPage, currentPageSize, currentSort, currentSortColumn} = this.state;
    let sortedData = objectsArraySort(
      this.props.data,
      currentSortColumn,
      currentSort
    );
    let result = sortedData.slice(currentPage * currentPageSize, (currentPage + 1) * currentPageSize);
    this.setState({
      currentData: result
    });
    this.gridScroller.current.scroll({
      top     : 0
    })
  };

  setPagination = (totalItemsCount, pageSize) => {
    const totalPages = Math.ceil(totalItemsCount / pageSize);
    const pages = [...Array(totalPages)].map((_, i) => i);
    this.setState({pages});
  };

  render() {
    const {pages, currentData, currentSort, currentSortColumn} = this.state;

    return (
      <div className="grid-container">
        <PageSize currentPageSize={this.state.currentPageSize}
                  handlePageSizeChange={this.handlePageSizeChange}/>

        <div className="grid-scroller"
             ref={this.gridScroller}>
          <Table data={currentData}
                 currentSort={currentSort}
                 currentSortColumn={currentSortColumn}
                 handleColumnSort={this.handleColumnSort}/>
        </div>

        <Pagination pages={pages}
                    currentPage={this.state.currentPage}
                    handlePageChange={this.handlePageChange}/>
      </div>
    );
  }
}

Grid.defaultProps = {
  initialPage: 1,
  pageSize   : 10
};

export default Grid;
