import React, { Component } from 'react';
import TABLE_COLUMNS_ENUM from '../../constants/tableColumnsEnum';
import './grid.css';

class Table extends Component {
  render() {
    return (
      <div className="table-container">
        <table cellPadding="0">
          <thead>
          <tr>
            <th onClick={() => this.props.handleColumnSort(TABLE_COLUMNS_ENUM['id'])}>
              <span>{TABLE_COLUMNS_ENUM['id']}</span></th>
            <th onClick={() => this.props.handleColumnSort(TABLE_COLUMNS_ENUM['postId'])}>
              <span>{TABLE_COLUMNS_ENUM['postId']}</span></th>
            <th onClick={() => this.props.handleColumnSort(TABLE_COLUMNS_ENUM['name'])}>
              <span>{TABLE_COLUMNS_ENUM['name']}</span></th>
            <th onClick={() => this.props.handleColumnSort(TABLE_COLUMNS_ENUM['email'])}>
              <span>{TABLE_COLUMNS_ENUM['email']}</span></th>
          </tr>
          </thead>
          <tbody>
          {this.props.data.map((item) => <TableRow key={'key' + item.id + item.postId} {...item} />)}
          </tbody>
        </table>
      </div>
    );
  }
}

const TableRow = ({postId, id, name, email}) => (
  <tr>
    <td width="10%">{postId}</td>
    <td width="10%">{id}</td>
    <td width="50%">{name}</td>
    <td width="30%">{email}</td>
  </tr>
);


export default Table;