const TABLE_COLUMNS_ENUM = {
  'id'    : 'id',
  'postId': 'postId',
  'name'  : 'name',
  'email' : 'email'
};

export default TABLE_COLUMNS_ENUM;