const TABLE_COLUMN_SORT_TYPES = {
  'ASC' : 0,
  'DESC': 1
};

export default TABLE_COLUMN_SORT_TYPES;