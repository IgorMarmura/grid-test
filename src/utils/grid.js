import TABLE_COLUMN_SORT_TYPES from '../constants/tableColumnsSortTypes';

export const objectsArraySort = (data, column, type) => {
  let temp = [...data];
  let sortFn = null;

  if(type === TABLE_COLUMN_SORT_TYPES.ASC) {
    sortFn = (a, b) => {
      if (a[column] > b[column]) {
        return 1;
      }
      if (a[column] < b[column]) {
        return -1;
      }
      return 0;
    }
  }
  if(type === TABLE_COLUMN_SORT_TYPES.DESC) {
    sortFn = (a, b) => {
      if (a[column] < b[column]) {
        return 1;
      }
      if (a[column] > b[column]) {
        return -1;
      }
      return 0;
    }
  }

  temp.sort(sortFn);
  return temp;
};